import * as ActionTypes from '../constants/action-types';
import SessionRecord from '../models/session';
import { parseFromServer as parseUser } from '../models/user';

const initialState = new SessionRecord();

// modify global state
export default function sessionReducer(session = initialState, action = {}) {
  switch (action.type) {

    // update user email
    case ActionTypes.CHANGE_USER_EMAIL_SUCCESS:
      return session.set('user', parseUser(action.result.user));

    default:
      return session;

  }
}
