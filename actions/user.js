import isEmpty from 'lodash/isEmpty';
import fromPairs from 'lodash/fromPairs';

import * as ActionTypes from '../constants/action-types';

export function updateEmail(email) {
  return async (dispatch, getState, apiClient) => {
    const state = getState();
    const { session } = state;
    try {
      dispatch({ type: ActionTypes.CHANGE_USER_EMAIL });
      const response = await apiClient.put(`/users/account/email/${session.user.id}/`, {
        data: { email },
      });
      dispatch({ type: ActionTypes.CHANGE_USER_EMAIL_SUCCESS, result: response.body });
    } catch (err) {
      if (err.messages && err.messages.length) {
        const serverFieldErrors = fromPairs(
          err.messages
            .filter(msg => msg.field !== 'null')
            .map(msg => [msg.field, processToken(msg.token)])
        );
        if (!isEmpty(serverFieldErrors)) {
          dispatch({
            type: ActionTypes.CHANGE_USER_EMAIL_FAILED,
            errors: serverFieldErrors,
          });

          err.updateEmail = true;
          err.fields = serverFieldErrors;

          throw err;
        }
      }

      dispatch({ type: ActionTypes.CHANGE_USER_EMAIL_FAILED, error: err });
    }
  };
}

function processToken(token, params) {
  switch (token) {
    case 'isEmpty':
      return 'Field is Required';
    case 'emailInUse':
      return 'Email in use';
    case 'verificationCodeExpirationTimeout':
      return 'Activation code has been resent within the past hour. If you do not receive this code, ' +
        `please resend again in ${params[0]} minutes`;
    case 'phoneNumberIsInvalid':
      return 'Phone number is invalid';
    case 'phoneCantBeUsed':
      return 'Your phone number cannot be used as the primary registration method in your region. ' +
        'Please enter an email address in order to register.';
    case 'phoneInUse':
      return 'The phone number you have entered is already in use within our system';
    default:
      return token;
  }
}
