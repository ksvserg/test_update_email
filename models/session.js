import Immutable from 'immutable';

import User, { parseFromServer as parseUser } from './user';

const { Record, Map } = Immutable;

const SessionRecord = Record({
  language: undefined, // current language is stored in session
  error: undefined,
  token: undefined,
  user: new User(),
});

export default class Session extends SessionRecord {
  isActiveUser() {
    return !!(this.user && this.user.id);
  }
}

export function parseFromServer(data) {
  return new Session({
    ...data,
    user: parseUser(data.user),
  });
}
