import Immutable from 'immutable';

const { Record, Map } = Immutable;

const UserRecord = Record({
  id: undefined,
  email: undefined,
  phone: undefined,
  phoneCountryCode: undefined,
  lastName: undefined,
  firstName: undefined,
  nickname: undefined,
  isActivationRequired: undefined,
  avatars: Map(),
});

export default class User extends UserRecord {
  isDeveloper() {
    return (!!this.email && this.email.endsWith('@bk.ru'));
  }
}

export function parseFromServer(serverData) {
  const data = { ...serverData };
  if (data.avatar && !data.avatars) {
    data.avatars = data.avatar;
  }
  return new User(data);
}
