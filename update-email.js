import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
} from 'react-native';


import { updateEmail } from './actions/user';

import isEmail from 'validator/lib/isEmail';
import Styles from 'styles/index';
import Colors from 'styles/palette';
// this files don't exist. But is it just simple regular React components
import Input from 'components/forms/input';
import Button from 'components/button';
import Page from 'components/page';

class UpdateEmailContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      errors: {},
      email: props.defEmail,
      saving: false,
    };
    this.onButtonPress = this.onButtonPress.bind(this);
  }

  onButtonPress() {
    const { email } = this.state;
    const { dispatch } = this.props;

    if (!email || !isEmail(email)) {
      this.setState({
        errors: {
          email: 'Email is invalid',
        },
      });
      return;
    }
    this.setState({ saving: true, errors: {} });

    dispatch(updateEmail(email)).then(() => {
      // success
      this.setState({ saving: false });
    }).catch(err => {
      if (err.updateEmail) {
        this.setState({ errors: err.fields, saving: false });
      }
    });
  }

  render() {
    // render screen with single input and button
    const { errors, saving } = this.state;
    const { defEmail } = this.props;
    return (
      <View style={Styles.flex}>
        <Page
          title="Update Email Address"
          color={Colors.green}
          onLeftIconPress={() => this.props.dispatch(goBack())}
        >
          <View style={styles.container}>
            <Input
              placeholder="Email"
              autoCorrect={false}
              keyboardType="email-address"
              onChangeText={val => this.setState({ email: val })}
              error={errors.email}
              defaultValue={defEmail}
            />

            <Button
              onPress={this.onButtonPress}
            >
              Save
            </Button>
          </View>
        </Page>
      </View>
    );
  }

}

UpdateEmailContainer.propTypes = {
  dispatch: PropTypes.func.isRequired,
  defEmail: PropTypes.string.isRequired,
};

// connect global state to controller
export default connect(state => ({
  defEmail: (state.session && state.session.user && state.session.user.email) || '',
}))(UpdateEmailContainer);

const styles = StyleSheet.create({
  container: {
    padding: 20,
    flex: 1,
  },
});
