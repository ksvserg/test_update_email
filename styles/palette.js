export default {

  red: '#be1d2c',
  darkRed: '#991824',
  lightGreen: '#7cab3a', // for favorites icon, success message
  green: '#608147', // for live text/bg and penalty bg
  veryLightGreen: '#a3be1c',
  darkGreen: '#597a2b',
  yellow: '#f0911b',
  lightYellow: '#fbf9ea', // for inputs background
  orange: '#ef901b', // for susp text/bg, notif bg
  darkOrange: '#c87d2f', // for susp text/bg, notif bg

  lightBlue: '#3a70a8', // notif bg
/*  blue: '#003d7d', // notif text */
  blue: '#3b71a9', // notif text
  brown: '#968051', // for event type button
  sea: '#01a89e', // subheader background color
  violet: '#9545c0',
  ochre: '#968052',
  cyan: '#47c8f5',
  rose: '#ec1e79',
  brownDark: '#3b1f1a',

  // Grays
  bgBlack: '#1a1a1a',
  black: '#0c0c0c',
  trueBlack: '#000000',
  darkestGray: '#262626', // footer and header bg, error messages bg
  darkerGray: '#333333', // text and icons, input text, input label text
  // match status panel bg

  darkGray: '#4d4d4d', // separators on dark bg in header/footer
  // input placeholders
  // player number bg
  gray: '#808080', // for "Or choose below" text in registration screen
  // for subtitles/secondary texts
  gray25: '#bfbfbf', // for dotted separators
  gray50: '#7f7f7f',
  // for subvalues in popup
  gray80: '#505050',

  gray12: '#e0e0e0', // for error messages and footer/header text,
  // borders and separators on light bg
  // input borders, input labels bg
  // player number text
  gray3: '#f7f7f7', // for page background
  grayMedal: '#ccc',
  white: '#ffffff',

  // Social
  facebook: '#526ca2',
  google: '#c83f2f',

  // Header gradient
  darkModerateBlue: '#285082',
  darkCyan: '#3c6e64',
  transparent: 'transparent',

  // contest medals
  bronze: '#a54',
  silver: '#688',
  gold: '#e0b24e',
};
