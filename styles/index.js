import { Platform, StyleSheet } from 'react-native';
import Colors from './palette';

const fontFamilyBase = Platform.OS === 'android' ? 'sans-serif-condensed' : 'HelveticaNeue-CondensedBold';
const fontFamilyLight = Platform.OS === 'android' ? 'sans-serif-condensed-light' : 'HelveticaNeue-Thin';
const fontSizeSmall = 14;
const fontSizeBase = 16;
const colorTextBase = Colors.darkerGray;

const styles = StyleSheet.create({
  padding21: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
    paddingBottom: 20,
  },
  p1: {
    padding: 10,
  },
  p2: {
    padding: 20,
  },
  pl1: {
    paddingLeft: 10,
  },
  pt1: {
    paddingTop: 10,
  },
  padding30: {
    paddingVertical: 30,
  },
  padding0020: {
    paddingBottom: 20,
  },
  margin0100: {
    marginRight: 10,
  },
  mt1: {
    marginTop: 10,
  },
  mt2: {
    marginTop: 20,
  },
  mb1: {
    marginBottom: 10,
  },
  mb2: {
    marginBottom: 20,
  },
  ml1: {
    marginLeft: 10,
  },

  letter: {
    backgroundColor: Colors.gray3,
    borderTopWidth: 1,
    borderTopColor: Colors.gray12,
    borderBottomWidth: 1,
    borderBottomColor: Colors.gray12,
    paddingLeft: 20,
  },
  letterText: {
    fontWeight: 'bold',
    fontSize: fontSizeBase,
    fontFamily: fontFamilyBase,
  },
  baseText: {
    color: colorTextBase,
    fontSize: fontSizeBase,
    fontFamily: fontFamilyBase,
  },
  smallText: {
    color: colorTextBase,
    fontSize: fontSizeSmall,
    fontFamily: fontFamilyBase,
  },
  lightText: {
    fontFamily: fontFamilyLight,
    fontWeight: Platform.OS === 'android' ? '100' : '300',
  },
  flex: {
    flexGrow: 1,
    flexShrink: 1,
  },
  bold: {
    fontWeight: 'bold',
  },
  link: {
    color: Colors.blue,
    textDecorationLine: 'underline',
  },
});

export default {
  borderRadius: 0, // New design w/o rounded corners

  colorTextBase,
  colorTextSecondary: Colors.gray,

  colorSep: Colors.gray12,
  colorSepDarkBg: Colors.darkGray,

  colorLightGreen: Colors.lightGreen,
  colorWhite: Colors.white,

  colorAppPageBackgroundColor: Colors.gray3,

  fontFamilyBase,

  // Font sizes
  fontSizeTiny: 11,
  fontSizeSmall,
  fontSizeBase,
  fontSizeTitle: 18,
  fontSizeButton: 20,
  fontSize20: 20,
  fontSize24: 24,
  fontSize32: 32,
  fontSize48: 48,

  // EventView shared variables for EventsList
  eventViewMargin: 10,
  eventViewTimeWidth: 62,
  eventViewIconWidth: 20,

  padding21: styles.padding21,
  p1: styles.p1,
  p2: styles.p2,
  pl1: styles.pl1,
  pt1: styles.pt1,
  padding30: styles.padding30,
  padding0020: styles.padding0020,
  margin0100: styles.margin0100,
  mt1: styles.mt1,
  mt2: styles.mt2,
  mb1: styles.mb1,
  mb2: styles.mb2,
  ml1: styles.ml1,
  letter: styles.letter,
  letterText: styles.letterText,
  baseText: styles.baseText,
  smallText: styles.smallText,
  lightText: styles.lightText,
  bold: styles.bold,

  flex: styles.flex,
  link: styles.link,
};
